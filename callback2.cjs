
/* 
    Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.
*/

const lists = require('./lists.json');

function callback2(boardID, callback) {
    if (boardID != undefined && typeof (boardID) == 'string' && callback != undefined && typeof (callback) == 'function' && boardID !== '') {

        setTimeout(() => {
            if (lists[boardID]) {
                callback(null, lists[boardID])
            } else {
                callback("boardId is not found")
            }

        }, 2 * 1000);

    } else {
        console.log('Invalid');
        return
    }

}



module.exports = callback2;