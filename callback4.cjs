/* 
    Problem 4: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind list simultaneously
*/

const callback1ThanosBoards = require("./callback1.cjs")
const callback2ThanosLists = require("./callback2.cjs")
const callback3MindListCards = require("./callback3.cjs")



function callback4(thanosID, listName) {

    setTimeout(() => {
        callback1ThanosBoards(thanosID, (err, data) => {
            if (err) {
                console.error(err);
            } else {
                console.log("info of thanos board");
                console.log(data, "data");
                callback2ThanosLists(thanosID, (err, listData) => {
                    if (err) {
                        console.error(err);
                    } else {
                        console.log("all lists for the thanos board")
                        console.log(listData);
                        const mindListId = listData.find((item, index) => {
                            if (item.name === listName) {
                                return item;
                            }
                        })
                        if (!mindListId) {
                            console.error("mind list not found")
                            return;
                        }
                        callback3MindListCards(mindListId.id, (err, cards) => {
                            if (err) {
                                console.error(err);
                            } else {
                                console.log("cards for the mind list")
                                console.log(cards);
                            }
                        })

                    }
                })
            }
        })
    }, 2 * 1000);
}




module.exports = callback4;