/* 
    Problem 6: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for all lists simultaneously
*/


const callback1ThanosBoards = require("./callback1.cjs")
const callback2ThanosBoardList = require("./callback2.cjs")
const callback3AllListCards = require("./callback3.cjs")

function callback6(thanosID) {

    setTimeout(() => {

        callback1ThanosBoards(thanosID, (err, boardData) => {

            if (err) {
                console.error(err)
                return
            } else {
                console.log("thanos board details")
                console.log(boardData)

                callback2ThanosBoardList(thanosID, (err, listData) => {

                    if (err) {
                        console.error(err)
                        return
                    } else {
                        console.log("Thanos Board List")
                        console.log(listData)

                        console.log("All Thanos List Cards data ")

                        for (let list of listData) {

                            callback3AllListCards(list.id, (err, eachCard) => {

                                if (err) {
                                    console.error(err)
                                    return
                                } else {
                                    console.log(list.name, "List Cards")
                                    console.log(eachCard)
                                }
                            })
                        }
                    }
                });
            }
        })
    }, 2 * 1000)
}



module.exports = callback6;