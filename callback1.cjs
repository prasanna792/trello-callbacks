/* 
    Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in boards.json and then pass control back to the code that called it by using a callback function.
*/

const boards = require('./boards.json');

function callback1(boardID, callback) {

    if (boardID != undefined && typeof (boardID) == 'string' && callback != undefined && typeof (callback) == 'function' && boardID != "") {

        setTimeout(() => {
            const requiredBoard = boards.find(board => {
                return board.id === boardID
            });

            if (requiredBoard) {
                callback(null, requiredBoard)
            } else {
                callback("boardID is not found")
            }
        }, 2 * 1000);

    } else {
        console.log('Invalid');
        return;
    }
}



module.exports = callback1;