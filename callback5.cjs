const callback1ThanosBoards = require("./callback1.cjs");
const callback2ThanosLists = require("./callback2.cjs");
const callback3MindListCards = require("./callback3.cjs");

function callback5(thanosID) {
    setTimeout(() => {
        callback1ThanosBoards(thanosID, (err, data) => {
            if (err) {
                console.error(err);
            } else {
                console.log("info of thanos board");
                console.log(data, "data");
                callback2ThanosLists(thanosID, (err, listData) => {
                    if (err) {
                        console.error(err);
                    } else {
                        console.log("all lists for the thanos board");
                        console.log(listData);
                        let listNames = ['Mind', 'Space'];

                        const result = listData.filter((item, index) => {
                            if (listNames.includes(item.name)) {
                                return item;
                            }
                        });

                        mindAndSpaceDetails(result, (err, data) => {
                            if (err) {
                                console.error(err);
                            } else {
                                console.log("final result");
                                console.log(data);
                            }
                        });
                    }
                });
            }
        });
    }, 2 * 1000);
}

function mindAndSpaceDetails(result, callback) {
    const cardDataArray = [];
    let counter = 0;

    for (let value of result) {
        callback3MindListCards(value.id, (err, data) => {
            if (err) {
                console.error(err);
                callback(err);
            } else {
                console.log("mind and space details");
                cardDataArray.push(data);

                counter++;
                if (counter === result.length) {

                    callback(null, cardDataArray);
                }
            }
        });
    }
}

module.exports = callback5;
