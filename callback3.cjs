/* 
    Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json. Then pass control back to the code that called it by using a callback function.
*/

const cards = require('./cards.json');

function callback3(listID, callback) {
    if (listID != undefined && typeof (listID) == 'string' && callback != undefined && typeof (callback) == 'function' && listID !== '') {
        setTimeout(() => {
            if (cards[listID]) {
                callback(null, cards[listID])
            } else {
                callback("listId is not found")
            }

        }, 2 * 1000)
    } else {
        console.log("Invalid")
        return;
    }
}

module.exports = callback3;