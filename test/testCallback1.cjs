const callback1 = require("../callback1.cjs")
const boardID = "mcu453ed"

callback1(boardID, (err, data) => {
    if (err) {
        console.error(err);
    } else {
        console.log(data);
    }
});

module.exports = callback1;